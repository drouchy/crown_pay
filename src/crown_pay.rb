# frozen_string_literal: true

require 'clamp'

module CrownPay
  module Gateway; end
  module Formatters; end
end

require_relative "./crown_pay/payment_processor"

Clamp do
  parameter "payment_file", "path to a csv file containing the payment definition to execute", attribute_name: :file

  option "--host", "host", "payment provider host", default: "http://localhost:7902"
  
  option "--account", "ACCOUNT", "API ACCOUNT", environment_variable: "API_ACCOUNT", required: true
  option "--api_key", "API_KEY", "API TOKEN", environment_variable: "API_KEY", required: true
  
  option "--timeout", "timeout", "maximum time (in s) to connect and receive a response from the payment provider", default: 10 do
    Integer(_1)
  end
  option "--verbose", :flag, "be chatty"
  
  option "--max_wait", "timeout", "maximum time (in s) to wait for a payment to be executed", default: 10 do
    Integer(_1)
   end
  
  def execute
    CrownPay::PaymentProcessor.new(host:, account:, api_key:, timeout:, max_wait:, verbose: verbose?).execute(file)
  end

end