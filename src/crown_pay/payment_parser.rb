# frozen_string_literal: true

require 'csv'
require_relative './models'
require_relative './errors'
require 'bigdecimal'

class CrownPay::PaymentParser
  def parse_file(file)
    CSV.read(file, headers: true).map do |row|
      CrownPay::PaymentDefinition.new(
        row['ID'],
        row['Recipient'],
        BigDecimal(row['Amount']),
        row['Currency']
      )
    end
  rescue Errno::ENOENT
    raise CrownPay::FileDoesNotExistError.new("File #{file} does not exist")
  rescue CSV::MalformedCSVError
    raise CrownPay::InvalidFile.new("File #{file} is not a valid CSV file")
  end
end