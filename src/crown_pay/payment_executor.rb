# frozen_string_literal: true

require_relative './gateway/payment'
require 'wait'

class CrownPay::PaymentExecutor
  def initialize(gateway, timeout: 10)
    @gateway = gateway
    @timeout = timeout
  end
  
  def execute(payment_definition)
    payment = gateway.execute(payment_definition)
    
    unless payment.fetch(:status) == :success
      return CrownPay::PaymentExecution.new(
        payment[:status],
        nil,
        nil,
        payment_definition.reference,
        payment[:status]
      )
    end
    
    execution = nil
    reference = payment.fetch(:reference)
    
    Wait.new(delay: 0.1, timeout: timeout).until do
      execution = gateway.check reference
      !execution.pending?
    end
    execution
  rescue Wait::ResultInvalid
    CrownPay::PaymentExecution.new(:timeout, nil, nil, payment_definition.reference, "The payment '#{payment_definition.reference}' is still pending")
  end
  
  private
  
  attr_reader :gateway, :timeout
end