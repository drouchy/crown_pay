# frozen_string_literal: true

require_relative './models'
require_relative './payment_parser'
require_relative './gateway/authentication'
require_relative './gateway/payment'
require_relative './payment_executor'
require_relative './formatters/csv_formatter'

class CrownPay::PaymentProcessor
  def initialize(host:, account:, api_key:, timeout:, max_wait:, verbose:)
    @config = CrownPay::Configuration.new
    config.from_args(host:, account:, api_key:, timeout:, max_wait:, verbose:)
    CrownPay.logger.level = :debug if verbose
  end
  
  def execute(file)
    executions = CrownPay::PaymentParser.new.parse_file(file).
      map { CrownPay::PaymentExecutor.new(payment_gateway, timeout: config.max_wait).execute(_1) }
      
    puts CrownPay::Formatters::CsvFormatter.new(executions).format
  end
  
  private
  
  attr_reader :config
  
  def bearer
    @bearer = authentication.bearer
  end
  
  def authentication
    @authentication ||= CrownPay::Gateway::Authentication.new(
      host: config.host, 
      timeout: config.timeout, 
      account: config.account, 
      api_key: config.api_key
    )
  end
  
  def payment_gateway
    @payment_gateway ||= CrownPay::Gateway::Payment.new(
      host: config.host, 
      timeout: config.timeout, 
      bearer: bearer
    )
  end
end
