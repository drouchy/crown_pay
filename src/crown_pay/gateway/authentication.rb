# frozen_string_literal: true

require 'faraday'
require_relative '../configuration'
require_relative '../models'

class CrownPay::Gateway::Authentication
  
  def initialize(host:, account:, api_key:, timeout: 20)
    @connection = Faraday.new(url: host, headers: { 'Api-key' => api_key }) do |faraday|
      faraday.response :logger, CrownPay.logger, { headers: true, bodies: true, errors: true }
      
      faraday.headers['Accept']       = 'application/json'
      faraday.headers['Content-Type'] = 'application/json'
      
      faraday.options.timeout = timeout
    end
    @account = account
  end
  
  def bearer
    response = @connection.post('/auth') do |request|
      request.body = {account: account}.to_json
    end
    
    JSON.parse(response.body)['token']
  rescue Faraday::TimeoutError
    raise CrownPay::TimeoutError
  rescue JSON::ParserError
    raise CrownPay::InvalidResponseError
  end
  
  private
  
  attr_reader :connection, :account
end 