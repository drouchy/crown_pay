# frozen_string_literal: true

require 'faraday'

class CrownPay::Gateway::Payment
  
  def initialize(host:, bearer:, timeout: 20)
    @connection = Faraday.new(url: host, headers: { 'Authorization' => "Bearer #{bearer}" }) do |faraday|
      faraday.response :logger, CrownPay.logger, { headers: true, bodies: true, errors: true }
      
      faraday.headers['Accept']       = 'application/json'
      faraday.headers['Content-Type'] = 'application/json'
      
      faraday.options.timeout = timeout
    end
  end

  
  def execute(payment)
    response = connection.post('/pay') do |req|
      req.body = {
        msisdn: payment.recipient,
        amount: payment.amount.to_i,
        currency:  payment.currency,
        reference:  payment.reference,
        url: ""
      }.to_json
    end
    
    unless response.status == 200
      return {status: :server_error}
    end
    
    body = JSON.parse(response.body)
    
    {status: STATUSES.fetch(body['status']), reference: body['conversationID']}
  end
  
  def check(payment_reference)
    response = connection.get("/status/#{payment_reference}")

    json = JSON.parse(response.body)

    CrownPay::PaymentExecution.new(
      STATUSES.fetch(json['status']),
      json['fee'] ? BigDecimal(json['fee'], 2) : nil,
      json['reference'],
      json['customerReference'],
      json['message']
    )
  end
  
  private
  
  attr_reader :connection
  
  STATUSES = {
    0     => :success,
    100   => :pending,
    1000  => :invalid_request,
    20000 => :invalid_phone_number,
    20001 => :recipient_account_locked,
    20002 => :sender_account_locked,
    20003 => :recipient_wallet_full,
    20004 => :insuffident_balance,
    20005 => :temporary_failure,
    20014 => :unregistered_phone_number,
    30006 => :duplicate_reference
  }
  
  def verbose? = true
end
