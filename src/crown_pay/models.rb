# frozen_string_literal: true

module CrownPay
  Configuration = Struct.new(:host, :account, :api_key, :timeout, :max_wait, :verbose) do
    def from_args(host:, account:, api_key:, timeout: 5, max_wait: 10, verbose: false)
      self.host = host
      self.account = account
      self.api_key = api_key
      self.timeout = timeout
      self.max_wait = max_wait
      self.verbose = verbose
    end
  end
  
  PaymentDefinition = Struct.new(:reference, :recipient, :amount, :currency)
  
  PaymentExecution = Struct.new(:status, :fee, :reference, :customer_reference, :message) do
    def pending? = status == :pending
  end
end
