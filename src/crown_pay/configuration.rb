# frozen_string_literal: true

require 'logger'
require_relative './models'

module CrownPay
  class << self
    attr_reader :configuration, :logger     
  end
  
  @logger = Logger.new($stderr, level: :error)
  @configuration = Configuration.new
end