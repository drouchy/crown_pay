# frozen_string_literal: true

module CrownPay
  class TimeoutError < RuntimeError; end
  class InvalidResponseError < RuntimeError; end
  class FileDoesNotExistError < RuntimeError; end
  class InvalidFile < RuntimeError; end
end