# frozen_string_literal: true

require 'faraday'

class CrownPay::Formatters::CsvFormatter
  def initialize(executions)
    @executions = executions
  end
  
  def format
    CSV.generate do |csv|
      csv << %w[ID Server-generated\ ID Status Fee Details]
      
      executions.each do |execution|
        csv << [
          execution.customer_reference,
          execution.reference,
          STATUSES.fetch(execution.status, "Unknown"),
          execution.fee&.to_f,
          execution.message
        ]
      end
    end
  end
  
  private
  
  attr_reader :executions
  
  STATUSES = {
    success: "Succeeded",
    invalid_request: "Failed",
    invalid_phone_number: "Failed",
    recipient_account_locked: "Failed",
    sender_account_locked: "Failed",
    recipient_wallet_full: "Failed",
    insuffident_balance: "Failed",
    temporary_failure: "Failed",
    unregistered_phone_number: "Failed",
    duplicate_reference: "Failed"
  }
end