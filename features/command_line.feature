Feature: Command Line
  
Scenario: Help
  When I run `pay --help` 
  Then the output should contain:
  """
  Usage:
      pay [OPTIONS] payment_file
  """