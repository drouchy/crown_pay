Feature: Execute payments
  
Background: "he environment variables are configured
  Given I set the environment variables to:
  | variable    |       value      |
  | API_KEY     | 0aVp83wuFp6wjvQ3 |
  | API_ACCOUNT | SEGOVIA          |
  
Scenario: Executing a single payments
  Given a file named "payments.csv" with:
  """
  ID,Recipient,Amount,Currency
  xxxxxxxx,254999999999,10,KES
  """
  When I run `pay payments.csv`
  Then the standard error should be blank
  Then the output should contain the csv:
  | ID       | Server-generated ID | Status    | Fee | Details                   | 
  | xxxxxxxx | XXXXXXXXX           | Succeeded | 0.1 | Succeeded with fee of 0.1 |
  
Scenario: Executing multiple payments
  Given a file named "payments.csv" with:
  """
  ID,Recipient,Amount,Currency
  aaaaaaaa,254999999998,10,KES
  bbbbbbbb,12125551000,20,GBP
  cccccccc,12125550000,10,GBP
  dddddddd,12125550001,10,GBP
  eeeeeeee,12125550002,10,GBP
  ffffffff,12125550003,10,GBP
  gggggggg,12125550004,10,USD
  hhhhhhhh,12125550005,10,USD
  iiiiiiii,12125550006,10,USD
  """
  When I run `pay --max_wait=10  payments.csv`
  Then the standard error should be blank
  Then the output should contain the csv:
  | ID       | Server-generated ID | Status    | Fee | Details                                 | 
  | aaaaaaaa | XXXXXXXXX           | Succeeded | 0.1 | Succeeded with fee of 0.1               | 
  | bbbbbbbb |                     | Unknown   |     | server_error                            | 
  | cccccccc |                     | Failed    |     | invalid_phone_number                    | 
  | dddddddd | XXXXXXXXX           | Failed    |     | RECIPIENT_ACCOUNT_LOCKED                | 
  | eeeeeeee | XXXXXXXXX           | Failed    |     | SENDER_ACCOUNT_LOCKED                   | 
  | ffffffff | XXXXXXXXX           | Failed    |     | RECIPIENT_WALLET_FULL                   | 
  | gggggggg | XXXXXXXXX           | Failed    |     | INSUFFICIENT_BALANCE                    | 
  | hhhhhhhh | XXXXXXXXX           | Failed    |     | TEMPORARY_FAILURE                       | 
  | iiiiiiii |                     | Failed    |     | duplicate_reference                     |
