require 'csv'

Then('the standard error should be blank') do
  expect(all_stderr.strip).to eq("")
end

Then('the output should contain the csv:') do |table|
  raw = table.raw
  output = CSV.parse(all_stdout.strip, headers: true)
  
  
  expect(output.count).to eq(raw.size - 1)
  expect(output.headers).to eq(raw.first)
  
  output.each_with_index do |row, index|
    table_row = raw[index +1]
    expect(row['ID'].strip).to eq table_row[0].strip
    if table_row[1] == 'XXXXXXXXX'
      expect(value(row, 'Server-generated ID')).to_not eq("")
    else
      expect(value(row, 'Server-generated ID')).to eq("")
    end
    expect(value(row, 'Status')).to eq table_row[2].strip
    expect(value(row, 'Fee')).to eq table_row[3].strip
    expect(value(row, 'Details')).to eq table_row[4].strip
  end
end

def value(row, attribute)
  (row[attribute] || "").strip
end