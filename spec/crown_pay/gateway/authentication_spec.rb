# frozen_string_literal: true

require 'spec_helper'
require 'bigdecimal'

RSpec.describe CrownPay::Gateway::Authentication do
  subject { described_class.new(host: host, account: account_name, api_key: secret) }
  
  let(:host) { 'http://localhost:7000' }
  
  let(:bearer_token) { 'BEARER_TOKEN' }
  let(:account_name) { "ACCOUNT_NAME" }
  let(:secret)       { "SECRET!" }
  
  describe '.bearer' do
    it 'calls the /auth endpoint to retrieve a bearer token' do
      stub_request(:post, "#{host}/auth").
        with(
          body: "{\"account\":\"#{account_name}\"}",
          headers: {
            'Accept'      => 'application/json',
            'Api-Key'     => secret,
            'Content-Type'=> 'application/json'
          }
        ).
        to_return(status: 200, body: { token: bearer_token}.to_json, headers: {'Content-Type' => 'application/json'})
        
      expect(subject.bearer).to eq(bearer_token)
    end
    
    context 'when the server does not reply some json' do
      
      it 'throws a custom error' do
        stub_request(:post, "#{host}/auth").to_return lambda { |request|
          { body: "xxxx", status: 200 }
        }
        
        expect { subject.bearer } .to raise_error(CrownPay::InvalidResponseError)
      end
    end
  end
end
