# frozen_string_literal: true

require 'spec_helper'
require 'bigdecimal'
require 'time'

RSpec.describe CrownPay::Gateway::Payment do
  subject { described_class.new(host: 'http://localhost:7000', bearer: bearer_token) }
  
  let(:bearer_token) { 'BEARER_TOKEN' }
  
  describe '.execute' do
    let(:request_body) do
      {
        "amount"=>10, 
        "currency"=>"GBP", 
        "msisdn"=>"000000001", 
        "reference"=>"the reference", 
        "url"=>""
      }
    end
    
    let(:payment) do
      CrownPay::PaymentDefinition.new("the reference", "000000001",  BigDecimal(10), "GBP")
    end
    
    it 'calls the /pay to execute a payment' do
      stub_request(:post, "http://localhost:7000/pay").
       with(
         body: request_body,
         headers: {
         'Authorization'=>'Bearer BEARER_TOKEN',
         'Content-Type'=>'application/json',
         }).
       to_return(status: 200, body: '{"status": 0,"conversationID": "167663707"}', headers: {'Content-Type' => 'application/json'})
      
      
      expect(subject.execute(payment)).to eq({status: :success, reference: '167663707'})
    end
  end
  
  describe '.check' do
    before do
      stub_request(:get, "http://localhost:7000/status/1234").
        with(
         headers: {
         'Accept'=>'application/json',
         'Authorization'=>'Bearer BEARER_TOKEN'
         }).
         to_return(status: 200, body: body.to_json, headers: {'Content-Type' => 'application/json'})
    end
    
    context "success" do
      let(:body) do
        {
          status: 0,
          timestamp: "2023-05-18T06:28:05Z",
          reference: "623653925",
          message: "Succeeded with fee of 0.1",
          customerReference: "the reference",
          fee: 0.1
        }
      end    
      
      it 'calls the API to check the payment status' do
        payment_execution = subject.check('1234')
        
        expect(payment_execution.status).to eq(:success)
        expect(payment_execution.fee).to eq(BigDecimal('0.1'))
        expect(payment_execution.reference).to eq('623653925')
        expect(payment_execution.customer_reference).to eq('the reference')
        expect(payment_execution.message).to eq('Succeeded with fee of 0.1')
      end
    end
    
    context "pending" do
      let(:body) do
        {
          status: 100,
          timestamp: "2023-05-18T06:28:05Z",
          reference: "623653925",
          customerReference: "the reference"
        }
      end
      
      it 'calls the API to check the payment status' do
        payment_execution = subject.check('1234')
        
        expect(payment_execution.status).to eq(:pending)
        expect(payment_execution.reference).to eq('623653925')
        expect(payment_execution.customer_reference).to eq('the reference')
        expect(payment_execution.fee).to be_nil
        expect(payment_execution.message).to be_nil
      end
    end
  end
end


