# frozen_string_literal: true

require 'spec_helper'
require 'bigdecimal'

RSpec.describe CrownPay::PaymentParser do
  subject { described_class.new }
  
  describe '.parse_file' do
    let(:file) { './spec/fixtures/sample-input.csv' }
    
    it 'parses the csv file and returns one element per valid line' do
      parsed = subject.parse_file(file)
      
      expect(parsed.count).to eq(4)
    end
    
    it 'parses the file and transform it into a model instance' do
      parsed = subject.parse_file(file).first
      
      expect(parsed.reference).to eq('aaaaaaaa')
      expect(parsed.recipient).to eq('254999999999')
      expect(parsed.amount).to eq(BigDecimal(10))
      expect(parsed.currency).to eq('KES')
    end
    
    context "the file does not exist" do
      let(:file) { "/dev/does_not_exist" }
      
      it "raises a specific error" do
        expect { subject.parse_file(file) }.to raise_error(CrownPay::FileDoesNotExistError)
      end
    end
    
    context "invalid csv file" do
      let(:file) { './spec/fixtures/invalid-input.csv' }
      
      it "raises a specific error" do
        expect { subject.parse_file(file) }.to raise_error(CrownPay::InvalidFile)
      end
    end
    
    pending "test valid csv but with an invalid format"
  end
end