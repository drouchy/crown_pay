# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CrownPay::PaymentExecutor do
  subject { described_class.new(gateway, timeout: 1) }
  
  let(:gateway) { double(:gateway) }
  
  describe '.execute' do
    let(:definition) do
      CrownPay::PaymentDefinition.new(
        'internal ref',
        'recipient',
        BigDecimal("10.3"),
        "GBP"
      )
    end
    
    let(:success) do
      CrownPay::PaymentExecution.new(
        :success, 
        BigDecimal("0.1"), 
        "ref", 
        "internal ref"
      )
    end
    
    let(:timeout) do
      CrownPay::PaymentExecution.new(
        :timeout, 
        nil,
        nil, 
        "internal ref",
        "The payment 'internal ref' is still pending"
      )
    end
    
    let(:pending) do
      CrownPay::PaymentExecution.new(:pending)
    end
    
    before do
      allow(gateway).to receive(:execute).and_return({status: :success, reference: '1234'})
    end
    
    it 'executes the payment, and wait for the sucess response' do
      allow(gateway).to receive(:check).with('1234').
        and_return(pending, pending, success)
        
      expect(subject.execute(definition)).to eq(success)
    end
    
    it 'returns a timeout execution if the payment is still pending after the timeout' do
      allow(gateway).to receive(:check).with('1234').and_return(pending)
      
      expect(subject.execute(definition)).to eq(timeout)
    end
  end
end