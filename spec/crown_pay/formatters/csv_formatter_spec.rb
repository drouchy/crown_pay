# frozen_string_literal: true

require 'spec_helper'

RSpec.describe CrownPay::Formatters::CsvFormatter do
  subject { described_class.new(executions) }
  
  describe ".format" do
    let(:executions) do
      [
        CrownPay::PaymentExecution.new(:success, 12.3, "the reference", "cust 1", "with 12.3 fee"),
        CrownPay::PaymentExecution.new(:insuffident_balance, nil, nil, "cust 2", "something went wrong"),
        CrownPay::PaymentExecution.new(:something_else, nil, nil, "cust 3", "server error")
      ]  
    end
    
    it "dumps the executions into a csv file" do
      expect(subject.format.strip).to eq("""ID,Server-generated ID,Status,Fee,Details
cust 1,the reference,Succeeded,12.3,with 12.3 fee
cust 2,,Failed,,something went wrong
cust 3,,Unknown,,server error""")
    end
    
    context "no executions" do
      let(:executions) { [] }
      
      it 'dumps an empty csv file - only the headers - when there is no executions' do
        expect(subject.format.strip).to eq("ID,Server-generated ID,Status,Fee,Details")
      end
    end
    
  end
end