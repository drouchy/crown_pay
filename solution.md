# Choices

I've implemented the solution using ruby for three main reasons:

* This is the language I've been using professionally for the last 18 months. While I can write code in Java, Kotlin, Elixir, Clojure, the fact that I've been using ruby on a daily basis makes me code faster.
* Ruby is an adequate choice to write a command-line tool thanks to the ecosystem, REPL
* There is something to be said about dynamically typed languages for API clients. The client does not control the types returned by the API, so having dynamic types makes it really easy to work with.

Given the time constraint, I've decided to focus on a simple, single-threaded solution with no callback support, but well tested. The main reason is that I think it's better to have something simple and readable as a foundation and work to improve performance later on. And thanks to the test coverage, it's easier to check if that breaks anything. "Build it, build it correctly, make it fast last".

I'm using two testing frameworks in the code:

* RSpec for unit tests in the spec directory
* cucumber for acceptance tests in the features directory

It might be a bit of an overkill there, but I chose that for time reasons. Cucumber and the gem Aruba are great to quickly bootstrap a command-line tool test.

This is the output of the acceptance test. It requires very little effort, and adds a lot of confidence in the code.

![Acceptance tests](https://gitlab.com/drouchy/crown_pay/-/raw/main/cuke.png "Acceptance tests")

# Requirements

* Ruby 3.1 and over
* bundler

# Setup

run `bundle install` to install all the dependencies


* clamp: command argument parser. Allows the tool to easily use environment variables, command arguments and flags 
* faraday: http client library. Use a unified API to use a vast number of HTTP client
* wait: allow to repeat a block of code until a condition is met or a time out exceeds. I'd not use this library for a real project, as it's an extra dependency for something we could easily write and test, but it's used there because of the time constraints.

* rspec: unit test framework
* cucumber: acceptance test framework
* aruba: command line test utilities
* webmock: http request mocking framework

# Usage

The command line usage is documented - thanks to clamp - 

`./bin/crown_pay --help`

```
Usage:
    pay [OPTIONS] payment_file

Parameters:
    payment_file          path to a csv file containing the payment definition to execute

Options:
    --host host           payment provider host (default: "http://localhost:7902")
    --account ACCOUNT     API ACCOUNT (required)
    --api_key API_KEY     API TOKEN (required)
    --timeout timeout     maximum time (in s) to connect and receive a response from the payment provider (default: 10)
    --verbose             be chatty
    --max_wait timeout    maximum time (in s) to wait for a payment to be executed (default: 10)
    -h, --help            print help
```

For example this is the command I used to test run the program

`API_KEY=0aVp83wuFp6wjvQ3 API_ACCOUNT=SEGOVIA ./bin/pay ./spec/fixtures/sample-input.csv`


# Architecture

I try to follow the SOLID principle. That means for a solution like that, each class has a single responsibility.

* gateway: API client - authentication to generate a bearer token - payment to manage payments
* payment_parser: parse the CSV file and transform it to the internal model
* payment_executor: code to instruct the API gateway to execute a payment
* formatters: format the result in the requested format. The solution only supports CSV
* payment_processor: orchestrate the whole process. There is no unit test for that class. I could not find a good way to unit test that kind of code, as it would require extensively using mocks, and then the unit test would only check the internal implementation and not the behaviour. The code is tested via the acceptance test, though


# If I had more time

My first choice would be to finish the actual implementation. The happy path is coded, some edge cases are there, but the code is missing testing really slow response, wrong host address... The message in case of an error are not human friendly. That should not be difficult, but the time was up.

My second choice would be to execute payments in parallel. Each payment batch is currently executed in sequence, and the code waits for a payment to be executed before moving on to the next one. There is a max_wait parameter to time out in case the payment takes too long to execute, but it's clearly not scalable.

Using a concurrent-map instead of a map while iterating over the payment definitions would be an obvious choice to me, as all the hard concurrency issues would be managed by the library.

Third choice would be to implement the callback. It won't be straightforward in ruby, but I could use a solution like using Sinatra to build an out of the box http server.

```ruby
require 'sinatra'

post '/callback/:reference' do
  # dispatch an callback event
end
```

An event could be an event-machine dispatch that the main process would listen to. The main process would need to use a thread-safe map to keep track the payment progress.